# flask_test.py
from flask import Flask, render_template, request
import requests

app = Flask(__name__)
# app = Flask(__name__, template_folder='.')
# https://riptutorial.com/flask/example/5303/render-template-usage

@app.route('/temperature', methods=['POST'])
def temperature():
    failure = False 
    city_name = request.form['zip']
    
    try:
        # Enter your API key
        r = requests.get('http://api.openweathermap.org/data/2.5/weather?'+ "appid=" + "d850f7f52bf19300a9eb4b0aa6b80f0d" + "&q=" + city_name)
        r.raise_for_status()
        json_object = r.json()
    except requests.exceptions.RequestException as err:
        message = "OOps something wrong connecting with openweathermap.org: " + str(err)
        failure = True
    # except requests.exceptions.HTTPError as errh:
    #    print ("Http Error:", errh)
    # except requests.exceptions.ConnectionError as errc:
    #    print ("Error Connecting:", errc)
    # except requests.exceptions.Timeout as errt:
    #    print ("Timeout Error:", errt)
    # except requests.exceptions.InvalidInvalidJSONError as errj:
    #    print ("JSON Error: ", errj)
    else:   
        try:
            temp_k = float(json_object['main']['temp'])
        except KeyError as erk:
            message = "OOps something wrong with the city: " + str(erk)
            failure = True            
        else:
            # Convert from kelvin to celsius
            temp_f = (temp_k - 273.15)
            temp_f = f'{temp_f:.2f}'            

    if failure:
        return render_template('error.html', info = message)
    else:
        return render_template('temperature.html', temp = temp_f, city = city_name)

@app.route('/')
def index():
    return render_template('index.html')

if __name__ == '__main__':
    # app.run(host, port, debug, options)
    # host: Hostname to listen on. Defaults to 127.0.0.1 (localhost). Set to ‘0.0.0.0’ to have server available externally
    app.run(host = '0.0.0.0', port = 5000, debug=True)
