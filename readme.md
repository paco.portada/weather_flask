## App creada con Flask para obtener la temperatura de una ciudad usando el [API de OpenWeatherMap](https://openweathermap.org/api)
 
&nbsp;

### Ejemplo extraído de [GeekyHumans.com](https://geekyhumans.com/get-weather-information-using-python/#Creating-a-Simple-Flask-Page-for-Showing-Weathernbsp)


<br>
instalar git
 
`sudo apt install git`
 
instalar python
 
`sudo apt install python3 python3-pip`

instalar Flask:

`pip install flask`

<br>
copiar los archivos:
 
`git clone https://gitlab.com/paco.portada/weather_flask.git`

<br>

ejecutar la app en flask:

`cd weather_flask`

`python3 flask_test.py`

<br>
abrir el puerto 5000 en el cortafuegos:

`sudo ufw allow 5000`

acceder en el navegador a la página principal en el servidor Flask:

[127.0.0.1:5000/](http://127.0.0.1:5000/)

[alumno.xyz:5000](https://alumno.xyz:5000)

<br>
  
introducir la ciudad:

![city](img/city.png)

   
<br>
Enviar la ciudad y recibir la temperatura:

![temp](img/city_temp.png)

 
<br> 
Error al obtener la ciudad:

![city_error](img/city_error.png)
  
<br>
